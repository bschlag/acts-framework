stages:
    - check
    - build
    - test

variables:
    BUILD_TYPE: Release
    GIT_SUBMODULE_STRATEGY: recursive

image: gitlab-registry.cern.ch/acts/machines/slc6:latest

before_script:
    # CI executor uses fail on error by default
    # setup scripts do not like that
    - set +e && source CI/setup_lcg94.sh; set -e

format:
  stage: check
  variables:
    GIT_SUBMODULE_STRATEGY: none # we don't need to check core submodule
  image: gitlab-registry.cern.ch/acts/machines/check:latest
  before_script: 
    - git submodule deinit -f external/*
  script:
    - CI/check_format .
  artifacts:
    paths:
      - changed
    when: on_failure

license:
  stage: check
  image: python:alpine3.6
  variables:
    GIT_SUBMODULE_STRATEGY: none # we don't need to check core submodule
  before_script:
    - apk --no-cache add git
    - git submodule deinit -f external/*
  script:
    - CI/check_license.py .

versions:
  stage: check
  variables:
    GIT_SUBMODULE_STRATEGY: none # we don't need to check core submodule
  tags:
    - cvmfs
  script:
    - ./CI/show_versions.sh

build:
  stage: build
  tags:
    - cvmfs
  script:
    - mkdir build
    - cd build
    - cmake -GNinja -DUSE_FATRAS=on -DUSE_TGEO=on -DUSE_DD4HEP=on -DUSE_GEANT4=on -DUSE_PYTHIA8=on -DCMAKE_INSTALL_PREFIX=../install -DCMAKE_BUILD_TYPE=${BUILD_TYPE} ..
    - cmake --build . -- install
  artifacts:
    paths:
      - build
      - install

build_vanilla:
  stage: build
  tags:
    - cvmfs
  script:
    - mkdir build
    - cd build
    - cmake -GNinja -DCMAKE_BUILD_TYPE=${BUILD_TYPE} ..
    - cmake --build . -- install

.examples: &example_base
  stage: test
  tags:
    - cvmfs
  before_script:
    - set +e && source CI/setup_lcg94.sh; set -e
    - export PATH=$PWD/build/bin:$PATH


hello_world:
  <<: *example_base
  script:
    - ACTFWHelloWorldExample

generic_propagation:
  <<: *example_base
  script:
    - ACTFWGenericPropagationExample

generic_fatras_example:
  <<: *example_base
  script:
    - ACTFWGenericFatrasExample
  allow_failure: true

root_fatras_example:
  <<: *example_base
  script:
    - ACTFWRootFatrasExample
  allow_failure: true

particle_gun_example:
  <<: *example_base
  script:
    - ACTFWParticleGunExample

whiteboard_example:
  <<: *example_base
  script:
    - ACTFWWhiteBoardExample

generic_geometry_example:
  <<: *example_base
  script:
    - ACTFWGenericGeometryExample

random_number_example:
  <<: *example_base
  script:
    - ACTFWRandomNumberExample

dd4hep_fatras_example:
  <<: *example_base
  script:
    - ACTFWDD4hepFatrasExample
  allow_failure: true

dd4hep_geometry_example:
  <<: *example_base
  script:
    - ACTFWDD4hepGeometryExample

dd4hep_propagation_example:
  <<: *example_base
  script:
    - ACTFWDD4hepPropagationExample

reproducibility_generic_propagation:
  stage: test
  tags:
    - cvmfs
  script:
    - source install/bin/setup.sh
    - cd scripts
    - ./testReproducibility.sh GenericPropagation 10 propagation-steps
